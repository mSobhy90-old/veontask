package msobhy.lib;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.TestScheduler;

/**
 * @author mSobhy
 */
public class TimerTest {
    /**
     * Results that appear after the second time advance by 50 ms (at 100ms).
     */
    private static final String FIRST_NUMBER_TO_EMMIT = "0";
    /**
     * Results that appear after the third time advance by 1 second (at 1s:100ms), due to already moving 100ms forward with the 2 previous time advancements 10 numbers should appear.
     */
    private static final String[] FIRST_BATCH_OF_RESULTS = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    /**
     * Results that appear after the fourth time advance by 1 second (at 2s:100ms).
     */
    private static final String[] SECOND_BATCH_OF_RESULTS = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"};

    @Test
    public void testGetNextNumberInSequence() {
        final TestScheduler testScheduler = new TestScheduler();
        final TestObserver<String> testObserver = new TestObserver<>();
        new Timer().getNextNumberInSequence(testScheduler)
                .observeOn(testScheduler)
                .subscribeOn(testScheduler)
                .subscribe(testObserver);
        testObserver.assertNoErrors();
        testObserver.assertNoValues();
        testScheduler.advanceTimeBy(50, TimeUnit.MILLISECONDS);
        testObserver.assertNoValues();
        testScheduler.advanceTimeBy(50, TimeUnit.MILLISECONDS);
        testObserver.assertValue(FIRST_NUMBER_TO_EMMIT);
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS);
        testObserver.assertValues(FIRST_BATCH_OF_RESULTS);
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS);
        testObserver.assertValues(SECOND_BATCH_OF_RESULTS);
        testObserver.onComplete();
        testObserver.assertComplete();
    }
}
