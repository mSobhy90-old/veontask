package msobhy.lib;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * A timer class that emits the next sequential value (starting from 0) every {@link #TIME_BETWEEN_EMISSIONS_IN_MILLIS}
 */
public class Timer {
    private final static int TIME_BETWEEN_EMISSIONS_IN_MILLIS = 100;

    /**
     * <b>Visible for testing</b> (as this is not an Android project and can't use {@code android.support.annotation.VisibleForTesting}
     * <br/>
     * Emits the next sequential value (starting from 0) every {@link #TIME_BETWEEN_EMISSIONS_IN_MILLIS}
     *
     * @param scheduler the Scheduler to use for scheduling the items
     * @return the Observable that will emmit the items
     */
    public Observable<String> getNextNumberInSequence(final Scheduler scheduler) {
        return Observable
                .interval(TIME_BETWEEN_EMISSIONS_IN_MILLIS, TimeUnit.MILLISECONDS, scheduler)
                .map(String::valueOf);
    }

    /**
     * Emits the next sequential value (starting from 0) every {@link #TIME_BETWEEN_EMISSIONS_IN_MILLIS}
     *
     * @return the Observable that will emmit the items
     */
    public Observable<String> getNextNumberInSequence() {
        return getNextNumberInSequence(Schedulers.computation());
    }
}
