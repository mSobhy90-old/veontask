#### Question:
Explain, in you own words, from the following code what thread the internal service call and the subsequent subscription is operating on when called from the main UI thread:

```java 
Observable.just(1)
        .subscribeOn(Schedulers.io())
        .switchMap(new Func1<Integer, Observable<Integer>>() {
            @Override
            public Observable<Integer> call(Integer integer) {
                return Observable
                        .just(100)
                        .subscribeOn(Schedulers.newThread());
            }
        })
        .subscribe(new Action1<Integer>() {
            @Override
            public void call(Integer integer) {
                // DO Something
            }
        });
```

#### Answer: 
So in this question we have 3 threads:
* UI thread (caller)
* IO Schedulers (from line 6)
* Thread X (from the `switchMap` `subscribeOn` call in line 12)
 
What will happen is that the Observable creation will happen on `UI thread` and then all upcoming operators will be on `IO thread` due to the `subscribeOn` call. So emitting 1 & `switchMap` will be emitting 100 in the `IO Thread`. `subscribeOn` after that will note that all upcoming operators should be done on `Thread X` and so anything happening inside the subscribe call will be on `Thread X`.
 
-----

#### Question:
Describe in as much detail as you feel necessary a high level architecture for a small news feed application. A news feed update can be notified to the user on a “push” basis as well as the user initiate a “pull” of updates to the feed.

#### Answer:
While I don't think writing is a good way to describe architecture, here we go: (BTW I have a hand drawing if you want I can share as well)

Note: Everything will be glued together using DI (Dagger 2).
* Data Layer (divided by technology) containing:
    * DTOs (API requests, responses classes):
        * `NewsFeedRequest`, `NewsFeedResponse`, etc...
    * Integration interfaces (since we'll use Retrofit with GSON & RxJava transformer)
        * `NewsFeedApi(page)`: which will return a `Single<NewsFeedResponse>` describing all the news feed articles in this page of the news feed.
    * DataSource's implementation (where the contract interfaces would be part of the Domain layer) wrapping the API integration or cache layer or persistable data, etc...
        * `NewsFeedDataSource implements NewsFeedDataSourceContract`: which will wrap the `NewsFeedApi`.
* Domain Layer (divided by feature) containing:
    * Contracts:
        * `NewsFeedDataSourceContract` which will have all the methods you would need while implementing the `NewsFeed` feature. To facilitate having a `CachedNewsFeedDataSource` in data layer that we can fetch from the cached data in case of rotation or going back and forth and waiting for the API to load.
    * Services:
        * `NewsFeedService`: to facilitate usage of `CachedNewsFeedDataSource` along with with `NewsFeedDataSource` from a single entry point.
    * Data models: to clean up the DTOs into a more usable object from a business perspective  
    * Converters: to convert from DTOs to DataModels.
* Presentation Layer (divided by feature) containing:
    * Transformers: to transform DataModels into UIModels 
    * UIModel: a transformed type of the `DataModel` that has all the data in a presentation ready state, (ex: date are all formatted Strings, and so on)
    * Adapters: 
        * `NewsFeedAdapter` that will hold `List<NewsFeedItemUiModel>` and will bind each and every one to a `ViewHolder` through `AdapterDelegate`. To be bound to the `RecyclerView`
        * `AdapterDelegate` that will decouple the `ViewHolder` binding from the adapter to be easily switchable later on.
        * `ViewHolder` to bind all data from one item into the view of this item
    * Views: The contract between `Presenters` and `Activity/Fragment`
    * Presenters: That is the one responsible for transforming the `DataModel` into presentable ready `UiModels` and handling all sort of events (including reacting on scroll event by loading next page using `NewsFeedService`) and so on. (Including saving and restoring state) 
    * Activity/Fragments: That only contain data/view binding and UI related logic and implements the view interface.

-----

#### Question:
In your own words explain what you dislike most about Android development and why?

#### Answer:
No platform is perfect, so the things I don't like so much about Android are as follows:
* Fragments are really easy to mess up, and people are doing a lot of effort to make sense out of them.
* UI testing is still really immature, which make's it hard to fully cover your app.
* No decent BDD framework, which makes integrating requirements into the app have contains a lot of guesswork and labor work.
* Too many device specs differ and lots of issues with platform fragmentation, for example devices that does not have Play Services.

But for me all that (and more) doesn't matter because Android has one of the best developer communities ever, very supportive community with a lot of GDE's. And a lot of awesome people doing a huge favor for developers with amazing open-source libraries.

-----