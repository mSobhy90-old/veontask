package msobhy.veontest.presentation.counter;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import javax.inject.Inject;

import butterknife.ButterKnife;

public abstract class BaseActivity<PresenterType extends BasePresenter<ViewType>, ViewType> extends AppCompatActivity {

    @Inject
    protected PresenterType presenter;

    private ViewType view;

    protected abstract void inject();

    @LayoutRes
    protected abstract int layoutToInflate();

    protected abstract ViewType getView();

    protected void restoreState(Bundle savedInstanceState) {
    }

    @Override
    @CallSuper
    protected void onCreate(Bundle savedInstanceState) {
        inject();

        super.onCreate(savedInstanceState);
        setContentView(layoutToInflate());

        ButterKnife.bind(this);

        view = getView();
        if (savedInstanceState != null) {
            restoreState(savedInstanceState);
        }
    }

    @Override
    @CallSuper
    protected void onResume() {
        super.onResume();
        presenter.attachView(view);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.detachView();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveState(outState);
    }

    protected void saveState(Bundle outState) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
