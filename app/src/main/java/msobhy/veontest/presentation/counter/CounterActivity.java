package msobhy.veontest.presentation.counter;

import android.widget.TextView;

import butterknife.BindView;
import msobhy.veontest.App;
import msobhy.veontest.R;

/**
 * An activity that shows a counter
 *
 * @author mSobhy
 */
public class CounterActivity extends BaseActivity<CounterPresenter, CounterView> implements CounterView {

    @BindView(R.id.first_subscriber_value)
    TextView firstSubscriberValueTextView;

    @BindView(R.id.second_subscriber_value)
    TextView secondSubscriberValueTextView;

    @BindView(R.id.third_subscriber_value)
    TextView thirdSubscriberValueTextView;


    @Override
    protected void inject() {
        App.getComponent().inject(this);
    }

    @Override
    protected int layoutToInflate() {
        return R.layout.activity_counter;
    }

    @Override
    protected CounterView getView() {
        return this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.init();
    }

    @Override
    public void updateFirstListener(String newCountString) {
        firstSubscriberValueTextView.setText(getResources().getString(R.string.counter_value, 1, newCountString));
    }

    @Override
    public void updateSecondListener(String newCountString) {
        secondSubscriberValueTextView.setText(getResources().getString(R.string.counter_value, 2, newCountString));
    }

    @Override
    public void updateThirdListener(String newCountString) {
        thirdSubscriberValueTextView.setText(getResources().getString(R.string.counter_value, 3, newCountString));
    }
}
