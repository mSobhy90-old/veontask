package msobhy.veontest.presentation.counter;

/**
 * The view contract between the UI view of the counter feature and the presenter
 *
 * @author mSobhy
 */
interface CounterView {
    void updateFirstListener(String newCountString);
    void updateSecondListener(String newCountString);
    void updateThirdListener(String newCountString);
}
