package msobhy.veontest.presentation.counter;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import msobhy.veontest.domain.counter.CounterDataSourceContract;

/**
 * Liaison between UI views and business logic for the counter feature
 *
 * @author mSobhy
 */
public class CounterPresenter extends BasePresenter<CounterView> {

    private final static int DELAY_TIME_IN_MILLIS = 200;

    private final CounterDataSourceContract counterDataSource;

    @Inject
    CounterPresenter(final CounterDataSourceContract counterDataSource) {
        this.counterDataSource = counterDataSource;
    }

    void init() {
        setupCounterFetching(number -> {
            if (getView() != null) {
                getView().updateFirstListener(number);
            }
        });
        setupCounterFetching(number -> {
            if (getView() != null) {
                getView().updateSecondListener(number);
            }
        });
        setupCounterFetching(number -> {
            if (getView() != null) {
                getView().updateThirdListener(number);
            }
        });
    }

    private void setupCounterFetching(Consumer<String> consumer) {
        addDisposable(counterDataSource.getNumber()
                .delay(DELAY_TIME_IN_MILLIS, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(consumer));
    }
}
