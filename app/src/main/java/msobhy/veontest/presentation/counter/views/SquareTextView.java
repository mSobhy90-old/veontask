package msobhy.veontest.presentation.counter.views;

import android.content.Context;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * A {@link android.widget.TextView} that's square at all times with a gradient background that switches automatically every {@link #BACKGROUND_SWITCH_INTERVAL}
 *
 * @author mSobhy
 */
public class SquareTextView extends AppCompatTextView {

    /**
     * Time taken while animating the background change in milliseconds
     */
    private final int ANIMATION_DURATION = 500;
    /**
     * Time between every background change in seconds
     */
    private final int BACKGROUND_SWITCH_INTERVAL = 2;

    public SquareTextView(Context context) {
        super(context);
        init(null);
    }

    public SquareTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SquareTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(@Nullable AttributeSet attrs) {
        setText(getText());

        Observable.interval(BACKGROUND_SWITCH_INTERVAL, TimeUnit.SECONDS)
                .map(value -> value % 4 == 0)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(notReverse -> {
                    final TransitionDrawable background = (TransitionDrawable) getBackground();
                    if (notReverse) {
                        background.startTransition(ANIMATION_DURATION);
                    } else {
                        background.reverseTransition(ANIMATION_DURATION);
                    }
                })
                .subscribe();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = Math.max(getMeasuredWidth(), getMinimumWidth());
        int height = Math.max(this.getMeasuredHeight(), getMinimumHeight());
        int size = Math.max(width, height);
        int widthSpec = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY);
        int heightSpec = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY);
        setMeasuredDimension(widthSpec, heightSpec);
    }
}