package msobhy.veontest.presentation.counter;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * @author mSobhy
 */
public class BasePresenter<ViewType> {

    private ViewType view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @CallSuper
    public void attachView(@NonNull final ViewType view) {
        this.view = view;
    }

    @CallSuper
    public void detachView() {
        view = null;
        compositeDisposable.clear();
    }

    /**
     * @return the attached view, In case no view is attach a null will be returned.
     */
    @Nullable
    protected ViewType getView() {
        return view;
    }

    protected final void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    public void restoreState(Bundle savedInstanceState) {

    }

    public void saveState(Bundle outState) {

    }
}