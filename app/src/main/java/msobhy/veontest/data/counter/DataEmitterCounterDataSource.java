package msobhy.veontest.data.counter;

import io.reactivex.Observable;
import msobhy.lib.Timer;
import msobhy.veontest.domain.counter.CounterDataSourceContract;

/**
 * Implementation of the {@link CounterDataSourceContract} using the data-emitter library
 *
 * @author mSobhy
 */
public class DataEmitterCounterDataSource implements CounterDataSourceContract {
    @Override
    public Observable<String> getNumber() {
        return new Timer().getNextNumberInSequence();
    }
}
