package msobhy.veontest.di;

import javax.inject.Singleton;

import dagger.Component;
import msobhy.veontest.App;
import msobhy.veontest.presentation.counter.CounterActivity;

@Singleton
@Component(
        modules = {
                ApplicationModule.class, DataSourceModule.class
        }
)
public interface ApplicationComponent {
    void inject(App app);

    void inject(CounterActivity counterActivity);
}
