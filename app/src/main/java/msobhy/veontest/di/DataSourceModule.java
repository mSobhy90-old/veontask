package msobhy.veontest.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import msobhy.veontest.data.counter.DataEmitterCounterDataSource;
import msobhy.veontest.domain.counter.CounterDataSourceContract;

/**
 * All data sources injection logic
 */
@Module
public class DataSourceModule {

    @Provides
    @Singleton
    public CounterDataSourceContract provideCounterDataSourceContract() {
        return new DataEmitterCounterDataSource();
    }
}
