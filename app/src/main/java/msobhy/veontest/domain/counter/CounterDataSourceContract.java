package msobhy.veontest.domain.counter;

import io.reactivex.Observable;

/**
 * The contract between presentation layer and data layer when it comes to getting the counter
 *
 * @author mSobhy
 */
public interface CounterDataSourceContract {
    Observable<String> getNumber();
}
