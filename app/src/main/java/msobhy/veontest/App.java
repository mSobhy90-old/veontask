package msobhy.veontest;

import android.app.Application;

import msobhy.veontest.di.ApplicationComponent;
import msobhy.veontest.di.ApplicationModule;
import msobhy.veontest.di.DaggerApplicationComponent;
import msobhy.veontest.di.DataSourceModule;

public class App extends Application {

    private static ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .dataSourceModule(new DataSourceModule())
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public static ApplicationComponent getComponent() {
        return applicationComponent;
    }
}
